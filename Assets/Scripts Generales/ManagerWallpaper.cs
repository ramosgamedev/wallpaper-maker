﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerWallpaper : MonoBehaviour
{
    public Transform CamaraParentX, camaraParentY;
    public float velocityRot = 10;
    public float MultiplyDistance = 3;

    public bool IsMove;
    public GameObject PointerMouse;
    public Camera CamaraMain;

    Vector3 centerPoint, pointMouse, lastPoint;
    bool setInStart;


    // Start is called before the first frame update
    void Start()
    {
        centerPoint = new Vector2(Screen.width / 2, Screen.height / 2);
    }

    // Update is called once per frame
    void Update()
    {
        if(!setInStart)
        {
            lastPoint = Input.mousePosition;
            setInStart = true;
        }

        if(Application.isEditor)
        {
            if(Input.GetKeyDown(KeyCode.P))
            {
                Debug.Break();
            }
        }

        if(IsMove)
        {
            pointMouse = Input.mousePosition - centerPoint;
            CamaraParentX.localPosition = Vector3.Slerp(CamaraParentX.localPosition, pointMouse * MultiplyDistance, Time.deltaTime * velocityRot);
            CamaraParentX.GetChild(0).LookAt(transform.position);
        }
        else
        {
            pointMouse = Input.mousePosition - lastPoint;
            lastPoint = Input.mousePosition;

            camaraParentY.Rotate(Vector3.up * pointMouse.x * velocityRot * Time.deltaTime);
            CamaraParentX.Rotate(Vector3.right * pointMouse.y * velocityRot * Time.deltaTime);
        }

        if(PointerMouse)
        {
            var v3 = Input.mousePosition;
            v3.z = 3.6f;
            //v3 = Camera.main.ScreenToWorldPoint(v3);

            Vector3 pointPos = CamaraMain.ScreenToWorldPoint(v3); //CamaraMain.ScreenToWorldPoint(Input.mousePosition);
            Debug.DrawRay(pointPos, Vector3.up, Color.red);

            //Vector3 localPOs = CamaraMain.transform.InverseTransformPoint(pointPos);
            //localPOs.z = 3.6f;
            //pointPos = CamaraMain.transform.TransformPoint(localPOs);

            PointerMouse.transform.position = pointPos;
            //PointerMouse.transform.localPosition = localPOs;
        }
    }
}
